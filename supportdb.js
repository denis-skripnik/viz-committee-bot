var Datastore = require('nedb')
  , db = new Datastore({ filename: 'support.db', autoload: true });
  db.persistence.setAutocompactionInterval(1000 * 1800);

function addSupportMSG(username, date, text) {
  return new Promise((resolve, reject) => {
  db.insert({login: username, datetime: date, text: text}, function (err, newDoc) {
if (err) {
  reject(err);
} else {
       resolve(newDoc);
}
    });
  });
  }

function findAllSupportMSG() {
  return new Promise((resolve, reject) => {
  db.find({}, (err, result) => {
if (err) {
  reject(err);
} else {
       resolve(result);
}
      });
});
}

module.exports.addSupportMSG = addSupportMSG;
module.exports.findAllSupportMSG = findAllSupportMSG;