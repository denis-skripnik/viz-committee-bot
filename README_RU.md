# viz-committee-bot
Английская версия: [README.md](README.md)
## Функционал бота:
1. Отправка уведомлений тем, кто им пользуется;
2. Возможность голосовать за заявки не выходя из него (**постинг ключ не сохраняется**),
3. Английская и Русская версия (команды Eng и Ru);
4. Возможность просмотра списка активных заявок (list).
5. Отправка администратором бота сообщений, например, с новостями, его пользователям (команда admin).
6. Поддержка: команда для отправки сообщений пользователями админу бота.
7. Список присланных сообщений (команда чат).

## Установка:
1. Загружаем папку на сервер;
2. выполняем
npm install
3. Меняем токен и admin_id на свои:
var token = 'sakdlsia:w6623';
var admin_id = 123456789;
4. Запускаем через pm2 start committeebot.js или node committeebot.

### Необходимое ПО:
1. Nodejs
2. npm.

## Автор бота - незрячий программист и делегат Денис Скрипник.
Аккаунт: https://viz.world/@denis-skripnik
Голосовать за делегата: https://viz.world/witnesses/denis-skripnik.