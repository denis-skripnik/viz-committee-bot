var Datastore = require('nedb');
  db_c = new Datastore({ filename: 'committee.db', autoload: true })
  db_c.persistence.setAutocompactionInterval(1000 * 1800);

  function getCommittee(list) {
    return new Promise((resolve, reject) => {
        db_c.findOne(list, (err,data) => {
               if(err) {
                      reject(err);
               } else {
                      resolve(data);
               }
        });
    });
}

function updateCommittee(old_list, last_list) {
  return new Promise((resolve, reject) => {
  db_c.update(old_list, last_list, {upsert:true}, (err, result) => {
if (err) {
  reject(err);
} else {
       resolve(result);
}
  });
  });
}

function findAllCommittee() {
  return new Promise((resolve, reject) => {
  db_c.find({}, (err, result) => {
if (err) {
  reject(err);
} else {
       resolve(result);
}
      });
});
}

module.exports.getCommittee = getCommittee;
module.exports.updateCommittee = updateCommittee;
module.exports.findAllCommittee = findAllCommittee;