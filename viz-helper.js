const botjs = require("./bot");
const cdb = require("./committeedb");
const view = require("./viewer");
var viz = require('viz-js-lib');
viz.config.set('websocket','wss://vizlite.lexai.host/');
viz.config.set('address_prefix','VIZ');
viz.config.set('chain_id','2040effda178d4fffff5eab7a915d4019879f5205cc5392e4bcced2b6edda0cd');


let committee_array = [];

async function workerVote(sender, num, percent, login, key) {
viz.broadcast.committeeVoteRequestAsync(key, login, num, percent, async (err, result) => {
await botjs.voteMSG(sender, err, result);
});
}




async function getCommitteeRequestsList(notes) {
  var workers_count = 0;
  const result = await viz.api.getCommitteeRequestsListAsync(0);
  const getworker = await cdb.getCommittee({});
  for (let application in result) {
    if (!getworker.committee.includes(result[application])) {
  await getCommitteeRequest(result[application], notes);
  workers_count += 1;
}
}
if (workers_count > 0) {
  await cdb.updateCommittee({}, {committee: result});
}
}

async function getCommitteeRequest(application, notes) {
  const res = await viz.api.getCommitteeRequestAsync(application, 0)
  const timezoneOffset = (new Date()).getTimezoneOffset() * 60000;
var timestamp = Date.parse(res.end_time);
const end_time = await view.date_str(timestamp - timezoneOffset, true, false, true);
for (var i = 0; i < notes.length; i++) {
  await botjs.msg(notes[i]['uid'], res, end_time);
}
}

async function getCommitteeRequestOne(application, uid) {
  const res = await viz.api.getCommitteeRequestAsync(application, 0)
const timezoneOffset = (new Date()).getTimezoneOffset() * 60000;
var timestamp = Date.parse(res.end_time);
const end_time = await view.date_str(timestamp - timezoneOffset, true, false, true);
  await botjs.oneUserMSG(uid, res, end_time);
}

module.exports.workerVote = workerVote;
        module.exports.getCommitteeRequestsList = getCommitteeRequestsList;
        module.exports.getCommitteeRequest = getCommitteeRequest;
        module.exports.getCommitteeRequestOne = getCommitteeRequestOne;