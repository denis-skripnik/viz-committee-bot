# viz-committee-bot
Russion version: [README_RU.md](README_RU.md)
## Bot functionality
    1. Sending notifications to those who use it
    2. Ability to vote for applications without leaving it (the posting key is not saved)
    3. English and Russian versions (Eng and Ru teams)
    4. Ability to view a list of active applications (list)
    5. Sending by admin bot messages, for example, with news, its users (admin command)
    6. Support: command to send message users to admin a bot
    7. List of sent messages (chat command)

## The installation:
    1. Upload the folder to the server;
    2. Perform
npm install
    3. Change the token and admin_id with your own:
var token = 'sakdlsia:w6623';
var admin_id = 123456789;
    4. Run through pm2 start committeebot.js or node committeebot.

## Software Required:
    1. Nodejs
    2. npm.

## The author of the bot is a the blind programmer and the delegate Denis Skripnik.
Account: https://viz.world/@denis-skripnik
Vote for witness: https://viz.world/witnesses/denis-skripnik.